**Minimal recurrent circuit for demonstrating recurrent network dynamics**

Part of the conference article:
*Teichmann, M., Hamker, F.H. (submitted). Learning Stable Recurrent Excitation in Simulated Biological Neural Networks. ICANN2017.*

The network implements recurrent excitation between two neurons, balanced by inhibitory inter neurons. It aims to elicit under which conditions such a network is stable by giving an analytical solution for its asymptotic activity.