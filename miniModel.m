%% Defining the network by its connectivity
% Basic recurrent excitation
w1=1;
w2=1;
% Lateral inhibition
w3=1;
w4=1;
% Excitatory feedback to inhibitory neurons
w5=1;
% Lateral inhibiton
c1=1;
c2=1;

%% Parameters
% ODE time constant
tau=10;
% Delay of the connections
d=1;

%% Simulation time
t=150;

%% Input strength
I = 0.5;

%% Calculate asymptotic activity for r_1
fprintf('Asymptotic activity of r_1 is %.3f\n',I/(1 - w1*w2/(1+w4*c2) + w3*c1 + w1*w5*c1/(1+w4*c2)))

%% Initial activities and preallocation
r1=zeros(1,t);
r2=zeros(1,t);
r3=zeros(1,t);
r4=zeros(1,t);

%% Main loop over time
for i=2:t
    
    %% Updating neurons having rectified linear activation function
    if (i-d)>0
        i_delayed=i-d;
    else
        i_delayed=1;
    end
    
    %r_1
    exc=I+r2(i_delayed)*w2;
    inh=r3(i_delayed)*c1;
    r1(i)=max(r1(i-1) + (exc - inh - r1(i-1))/tau,0);
    
    %r_2
    exc=r1(i_delayed)*w1;
    inh=r4(i_delayed)*c2;
    r2(i)=max(r2(i-1) + (exc - inh - r2(i-1))/tau,0);
    
    %r_3
    exc=r1(i_delayed)*w3+r2(i_delayed)*w5;
    r3(i)=max(r3(i-1) + (exc - r3(i-1))/tau,0);
    
    %r_4
    exc=r2(i_delayed)*w4;
    r4(i)=max(r4(i-1) + (exc - r4(i-1))/tau,0);
    
end

%% Visualization
plot(1:t,r1,1:t,r2,1:t,r3,'--',1:t,r4,'--','linewidth',2)
legend('r_1','r_2','r_3','r_4')
axis([1,t,0,inf])
xlabel('time')
ylabel('firing rate')